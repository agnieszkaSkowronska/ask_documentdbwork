﻿using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Stylelabs.Recruitment.Entities;
using System.Globalization;

namespace Stylelabs.Recruitment.DynamoDb.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Wire classes
            Application.Startup();

            // Get default culture
            var culture = CultureInfo.GetCultureInfo("en-US");

            // Create domain model consisting of a movie and actor definition
            var movieDefinition = new EntityDefinition
                {
                    Name = "Movie",
                    Labels = { 
                        new KeyValuePair<CultureInfo, string>(culture, "Movie Definition")
                    },
                    MemberGroups =
                    {
                        new MemberGroup
                        {
                            Name = "General",
                            MemberDefinitions =
                            {
                                new StringPropertyDefinition
                                {
                                    Name = "Title",
                                    IsMandatory = true,
                                    Labels = { { culture, "Title" } }
                                },
                                new DateTimePropertyDefinition
                                {
                                    Name = "ReleaseDate",
                                    IsMandatory = true,
                                    Labels = { { culture, "Release Date" } }
                                },
                                new StringPropertyDefinition
                                {
                                    Name = "StoryLine",
                                    Labels = { { culture, "StoryLine" } }
                                }
                            }
                         }
                    }
                };

            var actorDefinition = new EntityDefinition
                {
                    Name = "Actor",
                    Labels = { 
                            new KeyValuePair<CultureInfo, string>(culture, "Actor Definition")
                        },
                    MemberGroups =
                        {
                            new MemberGroup
                            {
                                Name = "General",
                                MemberDefinitions =
                                {
                                    new StringPropertyDefinition
                                    {
                                        Name = "Name",
                                        IsMandatory = true,
                                        Labels = { { culture, "Name" } }
                                    }
                                }
                            }
                        }
                };

            var entityFactory = Application.Container.Resolve<IEntityFactory>();

            // Create a movie with two actors
            var movie = entityFactory.Create(movieDefinition, culture);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);

            var actor1 = entityFactory.Create(actorDefinition, culture);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

            var actor2 = entityFactory.Create(actorDefinition, culture);
            actor2.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 2");
        }
    }
}
