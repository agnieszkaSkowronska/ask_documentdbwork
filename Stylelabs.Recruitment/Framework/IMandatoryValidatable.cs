﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// Indicates that the implementation can be mandatory.
    /// </summary>
    public interface IMandatoryValidatable
    {
        /// <summary>
        /// Gets or sets a value indicating whether the property mandatory.
        /// </summary>
        bool IsMandatory { get; set; }
    }
}
