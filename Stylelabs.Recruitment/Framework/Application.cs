﻿using Autofac;
using Stylelabs.Recruitment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public static class Application
    {
        public static IContainer Container { get; private set; }

        public static void Startup()
        {
            //Create new autofac container-builder.
            var containerBuilder = new ContainerBuilder();

            //Register all types
            containerBuilder.RegisterType<EntityFactory>().As<IEntityFactory>();
            containerBuilder.RegisterType<PropertyFactory>().As<IPropertyFactory>();
            containerBuilder.RegisterType<RelationFactory>().As<IRelationFactory>();

            Container = containerBuilder.Build();
        }
    }
}
