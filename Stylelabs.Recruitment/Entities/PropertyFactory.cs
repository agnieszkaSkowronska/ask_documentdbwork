﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Entities
{
    public class PropertyFactory : IPropertyFactory
    {
        #region Methods

        /// <summary>
        /// Creates a new instance of <see cref="Property" /> for the specified <paramref name="definition" />.
        /// The result will be a concrete implementation of <see cref="Property" />.
        /// </summary>
        /// <param name="definition">The definition to create a property for.</param>
        /// <returns>
        /// A concrete implementation of <see cref="Property" /> for the specified <paramref name="definition" />.
        /// </returns>
        public Property Create(PropertyDefinition definition)
        {
            Guard.ArgumentNotNull(definition, "definition");

            var result = ReflectionHelper.CreateGenericInstance(
                typeof(Property<>),
                definition.MultiValue
                    ? definition.DataType.MakeArrayType()
                    : definition.DataType) as Property;

            if (result == null)
                throw new InvalidOperationException("Unable to create an instance of Property for the specified PropertyDefinition.");

            result.Name = definition.Name;

            return result;
        }

        #endregion
    }
}
